#include <stdio.h>
#include <math.h>

float get_radius()
{
float radius;
printf ("enter radius\n");
scanf ("%f",&radius);
return radius;
}
float compute_circumferance(float radius)
{
return 2*M_PI*radius;
}
float compute_area(float radius)
{
float area;
area=M_PI*radius*radius;
return area;
}
void output(float radius,float area)
{
printf("the area of the circle with radius=%f is %f\n",radius,area);
}
void output_circumferance(float radius,float circumferance)
{
printf("the circumferance of the circle with radius %f is %f\n",radius,circumferance);
}
int main()
{
float radius,area,circumferance;
radius=get_radius();
area=compute_area(radius);
circumferance=compute_circumferance(radius);
output(radius,area);
output_circumferance(radius,circumferance);
return 0;
}
